# App

This project contains an AWS Lambda maven application with [AWS Java SDK 2.x](https://github.com/aws/aws-sdk-java-v2) dependencies.

## Prerequisites
- Java 1.8+
- Apache Maven
- [AWS SAM CLI](https://docs.aws.amazon.com/serverless-application-model/latest/developerguide/serverless-sam-cli-install.html)
- Docker

## Development

The generated function handler class just returns the input. The configured AWS Java SDK client is created in `DependencyFactory` class and you can 
add the code to interact with the SDK client based on your use case.

#### Building the project
```
mvn clean install
```

#### Testing it locally
```
sam local invoke
```

#### Adding more SDK clients
To add more service clients, you need to add the specific services modules in `pom.xml` and create the clients in `DependencyFactory` following the same 
pattern as dynamoDbClient.

## Deployment

The generated project contains a default [SAM template](https://docs.aws.amazon.com/serverless-application-model/latest/developerguide/sam-resource-function.html) file `template.yaml` where you can 
configure different properties of your lambda function such as memory size and timeout. You might also need to add specific policies to the lambda function
so that it can access other AWS resources.

To deploy the application, you can run the following command:

```
sam deploy --guided
```

See [Deploying Serverless Applications](https://docs.aws.amazon.com/serverless-application-model/latest/developerguide/serverless-deploying.html) for more info.

## index-service DDB patterns

To replace existing usages, the least ideal case is likely index-service, so
what does it do? I count nine patterns:

```
* get by pk+sk (only one)
getDuplicateForIncidentReports()
getAssessmentsByCorFStudent()
getStudentByCourseOrFolderAssessment()
getSummaryByCourseOrFolderAssessmentPostingStudent() NOTE: returns list
* get by pk (return first if more than one)
getAsessmentByStudentPosting()
* get by pk+begins_with(sk) (list)
getAssessmentsByCourse()
getAssessmentsByFolder()
getStudentsByFolder()
getStudentsByCourse()
getAssessmentsByFolderStudent()
getAssessmentsByCourseStudent()
getMinifiedStudentsPageByCorFAssessment()
getMinifiedStudentsPageByCorFAssessment()
getPostingsByCourseStudentAssessment()
getPostingsByFolderStudentAssessment()
* put by pk+sk (query+update or put new)
saveCourseOrFolderToAssessmentMapping() - turns out to be get+retrySaveWithBackoff()
* put by pk+sk (query or put new, no update if exists) - turns out to be get+retrySaveWithBackoff()
saveFolderStudentToAssessmentMapping()
saveCourseStudentToAssessmentMapping()
saveFolderAssessmentToStudentMapping()
saveCourseAssessmentToStudentMapping()
saveCourseStudentAssessmentToPosting()
saveFolderStudentAssessmentToPosting()
saveStudentPostingToCourseAssessmentMapping()
saveStudentPostingToFolderAssessmentMapping()
updateOrSaveCourseAssessmentToPostingStudentSummaryMapping()
updateOrSaveFolderAssessmentToPostingStudentSummaryMapping()
* put by pk+starts_with(sk) (despite begins_with, should be only 1) (query+update or put new)
updateOrSaveFolderToStudentMapping() - turns out to be get+retrySaveWithBackoff()
updateOrSaveCourseToStudentMapping() - turns out to be get+retrySaveWithBackoff()
* put by pk+starts_with(sk) (query+update, must exist) - turns out to be get+retrySaveWithBackoff()
updateAssessmentsByCourseOrFolderAndStudentDBWithInstitutionCourseFolderName()
updateAssessmentsByCourseOrFolderDBWithInstitutionCourseFolderName()
updateStudentsByCourseOrFolderAndAssessmentDBWithInstitutionCourseFolderName()
updateStudentsByCourseOrFolderDBWithInstitutionCourseFolderName()
updateStudentsSummaryPostingByCourseOrFolderAndAssessmentDBWithInstitutionCourseFolderName()
updateIndexServiceDBMap1WithInstitutionCourseFolderName()
updateIndexServiceDBMap2WithInstitutionCourseFolderName()
* mapper.save(o)
upsert()
retrySaveWithBackoff()
* table.updateItem(updateExpression)
setIfNotExists()
updateColumnByDelta()
updateColumn()
```

1. get by pk+sk, returns object
1. get by pk, returns object, first if more than one
1. get by pk+begins_with(sk), returns list of objects
1. put by pk+sk, put new if not existing, do nothing if existing
1. put by pk+begins_with(sk), query+update if existing, or put new (NOTE: Should not use begins_with!)
1. put by pk+begins_with(sk), query+update if existing, else do nothing (NOTE: Should not use begins_with)
1. mapper.save(object)
1. table.updateItem(updateExpression)

For 1. get by pk+sk, returns object:
```
// Supplied: pk (String), sk (String)
SimpleRecord readRecord = dynamoDbTable.getItem(Key.builder().partitionValue(pk).sortValue(sk).build());
```
For 2. get by pk, returns object, first if more than one:
```
// Supplied: pk (String)
SimpleRecord record = dynamoDbTable.query(r -> r.queryConditional(QueryConditional.sortBeginsWith(
        Key.builder().partitionValue(pk).build()
    )).limit(1)).items().stream().findFirst().orElse(null);
```
For 3. get by pk+begins_with(sk), returns list of objects:
```
// Supplied: pk (String), sk (String)
List<SimpleRecord> records = dynamoDbTable.query(QueryConditional.sortBeginsWith(
		Key.builder().partitionValue(pk).sortValue(sk).build())
).items().stream().collect(Collectors.toUnmodifiableList());
```
or
```
// Supplied: pk (String), sk (String)
PageIterable<SimpleRecord> records = dynamoDbTable.query(QueryConditional.sortBeginsWith(
        Key.builder().partitionValue(pk).sortValue(sk).build())));
records.stream().forEach(page -> page.items().forEach(record -> {
    logger.debug(record.getSk());
}));
```
For 4-6. all end up actually being first a get, then (sometimes) a retrySaveWithBackoff(), which does mapper.save()

For 7. mapper.save(object)
```
// Supplied: objectToSave (Object), invoker (String)
dynamoDbTable.put(objectToSave);
```
For 8. table.updateItem(updateExpression)
```
// NOTE: The atomic update feature is no longer available in SDKv2, although they've acknowledged it as a feature request.
// Instead we must get the item, update it in-place, and put it again. https://github.com/aws/aws-sdk-java-v2/issues/2292 
// For other update expressions, apparently we still have to supply the entire item? 
// Supplied: pk (String), sk (String), columnName (String), newValue (String/Integer/Long)
dynamoDbTable.updateItem(r -> r.item(writeRecord).conditionExpression(Expression.builder()
		.expression("set #p = :val")
		.putExpressionName("#p", columnName)
		.putExpressionValue(":val", AttributeValue.builder().n(String.valueOf(newValue)).build())
		.build()
).build());

```
