package com.turnitin;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyRequestEvent;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyResponseEvent;
import com.turnitin.dao.SimpleRecord;
import java.time.Instant;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import software.amazon.awssdk.enhanced.dynamodb.DynamoDbTable;
import software.amazon.awssdk.enhanced.dynamodb.Key;
import software.amazon.awssdk.enhanced.dynamodb.TableSchema;

public class App implements RequestHandler<APIGatewayProxyRequestEvent, APIGatewayProxyResponseEvent> {

	// Initialize the SDK client outside of the handler method
	private static final DynamoDbTable<SimpleRecord> dynamoDbTable = DependencyFactory.dynamoDbEnhancedClient(
			DependencyFactory.dynamoDbClient()).table(
			System.getenv("DYNAMO_TABLE"),
			TableSchema.fromBean(SimpleRecord.class));
	// TODO: Init turnitin.commons.context here, update everything below
	private static Logger logger = LoggerFactory.getLogger(App.class);
	//	private static final ObjectMapper objectMapper = ObjectMapperSingleton.getObjectMapper();

	static {
		// invoking a simple api here to pre-warm the application
		dynamoDbTable.getItem(Key.builder().partitionValue("warmup").build());
	}

	public App() {
	}

	@Override
	public APIGatewayProxyResponseEvent handleRequest(final APIGatewayProxyRequestEvent input, final Context context) {
		logger.info(String.format("Starting up with [%s] against path [%s]", input.getHttpMethod(), input.getPath()));
		logger.debug("Full input: " + input);
		APIGatewayProxyResponseEvent response = new APIGatewayProxyResponseEvent().withStatusCode(200)
				.withHeaders(Map.of("Content-Type", "application/json",
						"Access-Control-Allow-Origin", "*"))
				.withIsBase64Encoded(false)
				.withBody("{}");

		try {
			String dbkey = input.getPathParameters().get("dbkey");
			String value = input.getPathParameters().get("value");
			logger.debug(String.format("Params: dbkey [%s] value [%s]", dbkey, value));

			if (dbkey != null && !dbkey.isEmpty()) {
				switch (input.getHttpMethod()) {
					case "GET":
						Key key = Key.builder().partitionValue(dbkey).build();
						SimpleRecord readRecord = dynamoDbTable.getItem(key);
						response.setBody(String.valueOf(readRecord));
//						response.setBody(objectMapper.writeValueAsString(readRecord));
						break;
					case "POST":
						String body = input.getBody();
						logger.debug(String.format("POST body [%s]", body));
						SimpleRecord writeRecord = SimpleRecord.builder()
								.pk(dbkey)
								.sk(value)
								.type("type")
								.subType("subType")
								.created(Instant.now())
								.data(body)
								.build();
						dynamoDbTable.putItem(writeRecord);
						response.setBody(dbkey + "/" + value);
						break;
					default:
						throw new RuntimeException(input.getHttpMethod());
				}
				logger.info(String.format("Response body [%s]", response.getBody()));
			} else {
				logger.error("Unsupported query");
				response.setStatusCode(400);
			}
		} catch (final Exception e) {
			logger.error("ERROR: Lambda exits with exception: " + e);
			response.setStatusCode(500);
			response.setBody(e.getMessage());
		}
		return response;
	}
}
